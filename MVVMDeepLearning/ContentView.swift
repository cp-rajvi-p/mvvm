//
//  ContentView.swift
//  MVVMDeepLearning
//
//  Created by rajvi on 07/02/22.
//

import SwiftUI

struct ContentView: View {
    @State var countries = [Country]()
    
    var body: some View {
        NavigationView {
            List(countries, id: \.self) { country in
                HStack(spacing : 10 )
                {
                    Text(country.name)
                        .bold()
                    Spacer()
                    Text("\(country.population)")
                        .foregroundColor(.gray)
                }
            }
            .onAppear() {
                getCountries()
            }
        }.navigationTitle(Text("country"))
    }
}
extension ContentView {
    func getCountries() {
        ApiCall().getCountries { (result) in
            switch result {
            case .success(let countries):
                DispatchQueue.main.sync {
                    self.countries = countries
                }
            case .failure(let error) :
                print(error.localizedDescription)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
