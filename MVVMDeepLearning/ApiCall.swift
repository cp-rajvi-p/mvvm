//
//  Network.swift
//  MVVMDeepLearning
//
//  Created by rajvi on 07/02/22.
//

import Foundation

class ApiCall {
    func getCountries(completion: @escaping (Result<[Country],Error>)-> Void) {
        guard let url = URL(string: "https://restcountries.com/v2/all") else {
            print("Invalid URL!!"); return
        }
        URLSession.shared.dataTask(with: url) {
            (data, response , error ) in
            if let error = error {
                completion(.failure(error.localizedDescription as! Error))
            }
            do {
                let countries  = try! JSONDecoder().decode([Country].self, from: data!)
                completion(.success(countries))
                print(countries)
            } catch let jsonError {
                completion(.failure(jsonError.localizedDescription as! Error))
            }
        }.resume()
    }
}
