//
//  Country.swift
//  MVVMDeepLearning
//
//  Created by rajvi on 07/02/22.
//

import Foundation

struct Country: Codable, Hashable {
    let name: String
    let translations: [String: String?]
    let population: Int
    let flag: URL?
    let alpha3Code: Code
    
    typealias Code = String
}
