//
//  MVVMDeepLearningApp.swift
//  MVVMDeepLearning
//
//  Created by rajvi on 07/02/22.
//

import SwiftUI

@main
struct MVVMDeepLearningApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
